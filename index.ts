import * as express from 'express';
import * as morgan from 'morgan';
import * as mssql from 'mssql';
import * as moment from 'moment';
import * as soap from 'soap';
import * as queries from './queries';

const dpdConfig = {
	parcelStatusUrl: 'https://reg-prijemce.dpd.cz/Product_api/Product_api.svc?singleWsdl',
	username: 'tirelogi16',
	password: 'q6wr2'
}
const connectionConfig: mssql.config = {
	user: 'TireLogistics',
	password: 'tlTTG661',
	server: '192.168.30.192',
	database: 'DSB_LM'
};

const app = express();
app.use(morgan('dev'));

class Main {
	async start() {
		app.get('/parcel-status/dpd/:parcelNumber', async (req, res, next) => {
			if (req.header('X-Token') != 'e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855') {
				res.sendStatus(403);
			}

			try {
				let parcelNumber = req.params.parcelNumber;
				let parcelExists = await this.checkParcelExists(parcelNumber);

				if (!parcelExists) {
					res.json({ parcelNumber, parcelExists });
				} else {
					let parcelStatus = await this.getParcelStatus(parcelNumber);
					res.json({ parcelNumber, parcelExists, parcelStatus });
				}
			} catch (e) {
				next(e);
			}
		});

		// App default handlers
		app.use((req, res, next) => {
			res.status(404).send();
		});

		app.use((error, req, res, next) => {
			res.status(500).json({
				message: error.message
			});
		});

		let server = app.listen(3000, () => {
			console.log('Server running on port', server.address().port);
		});
	}

	private async checkParcelExists(parcelNumber: string) {
		let pool = new mssql.ConnectionPool(connectionConfig);
		await pool.connect();

		let parcelExists = await pool.request()
			.input('parcelNumber', mssql.VarChar, parcelNumber)
			.query(queries.parcelCheck);
		await pool.close();
		return parcelExists.recordset.length != 0;
	}

	private async getParcelStatus(parcelNumber: string) {
		let client = await soap.createClientAsync(dpdConfig.parcelStatusUrl);
		let getParcelStatus = await client.GetParcelStatusAsync({
			username: dpdConfig.username,
			password: dpdConfig.password,
			parcels: {
				'parcel:string': [parcelNumber]
			}
		});

		return getParcelStatus[0].GetParcelStatusResult;
	}
}

new Main().start();
