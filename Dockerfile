FROM node
EXPOSE 3000
WORKDIR /usr/src/app
COPY package.json package.json
RUN npm install
COPY . .
RUN npm run compile
CMD ["npm", "run", "start"]
