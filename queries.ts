export const parcelCheck = `
--DECLARE @parcelNumber varchar(max) = '13915026458011'

SELECT TOP 1 1
FROM Preprava_cisla_baliku WITH(NOLOCK)
WHERE LEFT(Kod_rady_prepravy, 4) = 'TASY'
	AND Odeslano_prepravci = 1
	AND Cislo_baliku = @parcelNumber`;
